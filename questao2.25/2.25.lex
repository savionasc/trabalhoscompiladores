%{
	#include <stdio.h>
	#ifndef FALSE
	#define FALSE 0
	#endif
	#ifndef TRUE
	#define TRUE 1
	#endif
	int inComment = FALSE;

	#ifndef yywrap
	static int yywrap (void) { return 1; }
	#endif
%}

letra [A-Z]

%%

letra { 
		if(!inComment){
			putchar(tolower(yytext[0]));
		}else{
			putchar(yytext[0]);
		}

	}

"/*" {
	ECHO;
	inComment = TRUE;
	}

"*/" {
	ECHO;
	inComment = FALSE;
	} 


%%

void main(void){
	yylex();
}

