#include <stdio.h>
#include <stdlib.h>

char token;

float exp(void);
float term(void);
float factor(void);

void error(void){
	fprintf(stderr, "Error\n");
	exit(1);
}

float potencia(float n, float x){
	float res = 1.0;
	if(x == 0){
		return 1;
	}
	if (x < 0)
		error();

	for (int i = x; i >= 1; i--){
		res *= n;
	}
	return res;
}

void match(char expectedToken){
	printf("d");
	if(token == expectedToken)
		token = getchar();
	else{
		printf("j");
		error();
	}
}

float exp(void){
	printf("c");
	float temp = term();
	while ((token == '+') || (token=='-'))
		switch(token){
			case '+' :
				match('+');
				temp+=term();
				break;
			case '-':
				match('-');
				temp-=term();
				break;
		}
		return temp;
}

float exfactor(void){
	float temp = factor();
	switch(token){
		case '^':
			match('^');
			temp = potencia(temp, factor());
			break;
	}
	return temp;

}

float term(void){
	printf("b");
	float temp = exfactor();
	while((token=='*') || (token == '/')|| (token == '%')){
		switch(token){
			case '*':
				match('*');
				temp *= exfactor();
				break;
			case '/':
				match('/');
				temp = temp/exfactor();
				break;
			case '%':
				match('%');
				temp = (float) ((int)temp % (int) exfactor());
				break;

		}
	}
	return temp;
}

float factor(void){
	printf("a");

	float temp;
	if(token == '('){
		printf("e");
		match('(');
		temp = exp();
		match(')');
	}
	else if(isdigit(token) || token == '-'){
		printf("%c", token);
		ungetc(token, stdin);
		scanf("%f", &temp);
		token = getchar();
	}
	else{
		printf("h");
		error();
	}
	return temp;
}

int main(){
	float result;
	token = getchar();
	result = exp();

	printf("%c\n", token);

	if(token == '\n')
		printf("Result = %f\n", result);
	else{
		printf("X\n");
		error();
	}
	return 0;
}
