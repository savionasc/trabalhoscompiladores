#include <stdio.h>
#include <stdlib.h>

char token;

int exp(void);
int term(void);
int fator(void);

void error(void){
	fprintf(stderr, "Error\n");
	exit(1);
}

int potencia(int n, int x){
	int res = 1;
	if(x == 0){
		return 1;
	}
	if (x < 0)
		error();

	for (int i = x; i >= 1; i--){
		res *= n;
	}
	return res;
}

void match(char expectedToken){
	printf("d");
	if(token == expectedToken)
		token = getchar();
	else{
		printf("j");
		error();
	}
}

int exp(void){
	printf("c");
	int temp = term();
	while ((token == '+') || (token=='-'))
		switch(token){
			case '+' :
				match('+');
				temp+=term();
				break;
			case '-':
				match('-');
				temp-=term();
				break;
		}
		return temp;
}

int exfactor(void){
	int temp = factor();
	switch(token){
		case '^':
			match('^');
			temp = potencia(temp, factor());
			break;
	}
	return temp;

}

int term(void){
	printf("b");
	int temp = exfactor();
	while((token=='*') || (token == '/')|| (token == '%')){
		switch(token){
			case '*':
				match('*');
				temp *= exfactor();
				break;
			case '/':
				match('/');
				temp = temp/exfactor();
				break;
			case '%':
				match('%');
				temp = temp % exfactor();
				break;

		}
	}
	return temp;
}

int factor(void){
	printf("a");

	int temp;
	if(token == '('){
		printf("e");
			('(');
		temp = exp();
		match(')');
	}else if(isdigit(token) || token == '-'){
		printf("%c", token);
		ungetc(token, stdin);
		scanf("%d", &temp);
		token = getchar();
	}
	else{
		printf("h");
		error();
	}
	printf("temp:%d\n", temp);
	return temp;
}

int main(){
	int result;
	token = getchar();
	result = exp();

	printf("%c\n", token);

	if(token == '\n')
		printf("Result = %d\n", result);
	else{
		printf("X\n");
		error();
	}
	return 0;
}
