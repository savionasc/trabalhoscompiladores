%{
	#include <stdio.h>
	
	#ifndef FALSE
	#define FALSE 0
	#endif
	#ifndef TRUE
	#define TRUE 1
	#endif
	#ifndef yywrap
	static int yywrap (void) { return 1; }
	#endif

%}

%% 


"/*" {
 	char lido;
	int verifica = FALSE;
	ECHO;
	do{	
		while((lido=input())!='*')
			putchar(toupper(lido));
		putchar(toupper(lido));
		while((lido=input())=='*')
			putchar(toupper(lido));
		putchar(toupper(lido));

		if(lido == '/') verifica = TRUE;

	}while(!verifica);
	
}

%%

int main(int argc, char const *argv[]){
	yylex();
}
