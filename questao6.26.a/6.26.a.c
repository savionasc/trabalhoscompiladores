/* Calculadora para aritmética de inteiras simples segundo a EBNF:

<exp> -> <termo> {<soma> <termo>}
<soma> -> + | -
<termo> -> <factor> {<mult> <factor>}
<mult> -> *
<factor> -> ( <exp> ) | Número

Linhas de texto fornecidas como entrada de stdin
Imprime o resultado ou então "Error".
*/

#include <stdio.h>
#include <stdlib.h>

char token; /* variável de marca global */

/* protótipos de funções para ativações recursivas */
int exp(void);
int term(void);
int factor(void);

char resultS[100];
int i = 0;

void GravarNumero(char c){
	resultS[i] = c;
	i++;
}

void error(void){
	fprintf(stderr, "Error\n");
	exit(1);
}

void match(char expectedToken){
	if (token = expectedToken) token = getchar();
	else error();
}

int main(int argc, char const *argv[])
{
	token = getchar(); /* carga de marca com primeiro caractere para verificação à frente */
	GravarNumero(token);

	exp();
	if (token == '\n') {/* teste final de linha */
		printf("Result pos-fixo: %s\n",resultS );
	}
	else error(); /* caracteres indevidos na linha */
	return 0;
}

int exp(void){
	int temp = term();
	int aux = temp;
	char aux2;
	while ((token == '+') || (token == '-')){
		switch(token){
			case '+':
				aux2 = token;
				match('+');
				aux = term();
				//temp+=aux;

				GravarNumero((aux+48));
				GravarNumero(aux2);
				break;
			case '-':
				aux2 = token;
				match('-');
				aux = term();
				//temp-=aux;

				GravarNumero((aux+48));
				GravarNumero(aux2);
				break;
		}
	}
	return temp;
}


int term(void){
	int temp = factor();
	int aux = temp;
	char aux2;
	while (token == '*'){
		aux2 = token;
		GravarNumero((temp+48));
		GravarNumero(token);
		match('*');
		aux = factor();
		//temp*= aux;

		GravarNumero((aux+48));
		GravarNumero(aux2);
	}
	return temp;
}

int factor(void){
	int temp;
	if (token == '('){
		GravarNumero(token);
		match('(');
		temp = exp();
		match(')');
		GravarNumero(token);
	}
	else if( isdigit(token)){
		ungetc(token,stdin);
		scanf("%d", &temp);
		token = getchar();
	}
	else error();
	return temp;
}