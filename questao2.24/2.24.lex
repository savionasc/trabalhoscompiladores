%{
	#include <stdio.h>
	#include <string.h>	
	int ctLinha = 1;
	int ctPalavra = 0;
	int ctLetra = 0;
	#ifndef yywrap
	static int yywrap (void) { return 1; }
	#endif
%}

linha \n

digit [0-9]
numero {digit}+
letra [A-Za-z]
word ({letra}+|{letra}+{numero}|{numero})
outro [}{)(;=#*/! 	]
%% 

{linha} {ctLinha++;}
{word} {ctPalavra++; ctLetra += strlen(yytext);}
{outro} {}

%%

void main(void){	
	yylex();

	FILE * pFile;
	pFile = fopen ("saida.txt","w");

	fprintf(pFile, "Letras: %d\nPalavras:  %d\nLinhas: %d\n", ctPalavra,ctLetra, ctLinha);
}
